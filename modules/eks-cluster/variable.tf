variable "cidr_block" {
  description = "CIDR Network IP"
  type        = string
}

variable "vpc_name" {
  description = "vpc name"
  type        = string
}

variable "cidr_pb-1" {
  description = "CIDR Network IP"
  type        = string
}

variable "cidr_pb-2" {
  description = "CIDR Network IP"
  type        = string
}

variable "cidr_pv-1" {
  description = "CIDR Network IP"
  type        = string
}

variable "cidr_pv-2" {
  description = "CIDR Network IP"
  type        = string
}


variable "cluster_name" {
  description = "Cluster Name"
  type        = string
}

variable "cluster_version" {
  description = "version of EKS cluster"
  type        = string
}

