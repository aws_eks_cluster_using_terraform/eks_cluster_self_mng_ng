
output "cluster_endpoint" {
    value = aws_eks_cluster.eks.endpoint
 }

output "aws_subnet_public-1" {
    value = aws_subnet.public-1.id
 }

output "aws_subnet_public-2" {
    value = aws_subnet.public-2.id
 }

output "cluster_sg_id" {
    value = aws_eks_cluster.eks.vpc_config[0].cluster_security_group_id
 }

output "aws_sg_id" {
    value = aws_security_group.allow_tls.id
 }

