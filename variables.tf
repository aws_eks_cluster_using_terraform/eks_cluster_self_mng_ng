variable "cidr_block" {
  description = "CIDR Network IP"
  type        = string
  default     = ""
}

variable "vpc_name" {
  description = "vpc name"
  type        = string
  default     = ""
}

variable "cidr_pb-1" {
  description = "CIDR Network IP"
  type        = string
  default     = ""
}

variable "cidr_pb-2" {
  description = "CIDR Network IP"
  type        = string
  default     = ""
}

variable "cidr_pv-1" {
  description = "CIDR Network IP"
  type        = string
  default     = ""
}

variable "cidr_pv-2" {
  description = "CIDR Network IP"
  type        = string
  default     = ""
}


variable "cluster_name" {
  description = "Cluster Name"
  type        = string
  default     = ""
}


variable "cluster_version" {
  description = "version of EKS cluster"
  type        = string
  default     = ""
}

variable "node_group_name" {
  description = "Name of node group"
  type        = string
  default     = ""
}

variable "instance_type" {
  description = "Instance type of Node Group"
  type        = string
  default     = ""
}

variable "desired_size" {
  description = "Instance desired scaling of Node Group"
  type        = string
  default     = ""
}

variable "max_size" {
  description = "Instance max scaling of Node Group"
  type        = string
  default     = ""
}

variable "min_size" {
  description = "Instance min scaling of Node Group"
  type        = string
  default     = ""
}

