
module "eks_cluster" {
  source          = "./modules/eks-cluster"
  cidr_block      = var.cidr_block
  vpc_name        = var.vpc_name	
  cidr_pb-1       = var.cidr_pb-1
  cidr_pb-2       = var.cidr_pb-2
  cidr_pv-1       = var.cidr_pv-1
  cidr_pv-2       = var.cidr_pv-2
  cluster_name    = var.cluster_name
  cluster_version = var.cluster_version
}

module "self_managed_node_group" {
  source = "./modules/self-managed-node-group"

  name                   = var.node_group_name
  cluster_name           = var.cluster_name
  cluster_version        = var.cluster_version
  cluster_endpoint       = module.eks_cluster.cluster_endpoint
  subnet_ids             = [module.eks_cluster.aws_subnet_public-1, module.eks_cluster.aws_subnet_public-2]
  vpc_security_group_ids = [module.eks_cluster.cluster_sg_id, module.eks_cluster.aws_sg_id,]
  min_size               = var.min_size
  max_size               = var.max_size
  desired_size           = var.desired_size
  launch_template_name   = "separate-self-mng"
  instance_type          = var.instance_type

  tags = {
    Environment = "dev"
    Terraform   = "true"
  }
}

